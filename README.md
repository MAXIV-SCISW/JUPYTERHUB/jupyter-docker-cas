# JUPYTERHUB CAS AUTHENTICATION

## OVERVIEW

This is a jupyterhub extension that allows CAS authenticaion to be used.

This code is a modified version of
[jhub_cas_authenticator](https://github.com/cwaldbieser/jhub_cas_authenticator).

It seems like there were some mistakes in the jhub_cas_authenticator code as
well as some incompatibilities with the MAX IV CAS server. I think it's all be
sorted out now in this repository.

## USING CODE

This code should be installed into the jupyterhub container, like so:
```bash
cd /path/to/jupyter-docker-hub/
vim Dockerfile
RUN pip install \
    git+https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-docker-swarmspawner.git@master
```

The jupyterhub config file used by the jupyterhub docker service needs to also
modified:
```bash
cd /path/to/jupyter-docker-service/
vim maxiv/jupyterhub_config.py
    # Use CAS for authentication
    c.JupyterHub.authenticator_class = \
        'jhub_cas_authenticator.cas_auth.CASLocalAuthenticator'

    # The CAS URLs for logging in, out, and validating tickets
    c.CASLocalAuthenticator.cas_login_url = 'https://cas.maxiv.lu.se/cas/login'
    c.CASLocalAuthenticator.cas_logout_url = 'https://cas.maxiv.lu.se/cas/logout'
    c.CASLocalAuthenticator.cas_service_validate_url = \
        'https://cas.maxiv.lu.se/cas/p3/serviceValidate'

    # The service URL the CAS server will redirect the browser back to upon
    # successful authentication. The port here needs to match the exposed port in
    # docker-compose.yml
    c.CASLocalAuthenticator.cas_service_url = \
        'https://hdf5view.maxiv.lu.se:8443/base_url/hub/login'

    # A set of attribute name and value tuples a user must have to be allowed
    # access.
    c.CASLocalAuthenticator.cas_required_attribs = {('memberOf', 'Staff')}

    # Create system users just-in-time.
    c.CASLocalAuthenticator.create_system_users = True
```
