#!/usr/bin/env python
# coding: utf-8

import os
from setuptools import setup

pjoin = os.path.join
here = os.path.abspath(os.path.dirname(__file__))

# Get the current package version.
version_ns = {}
with open(pjoin(here, 'version.py')) as f:
    exec(f.read(), {}, version_ns)

setup_args = dict(
    name='jhub_cas_authenticator',
    packages=['jhub_cas_authenticator'],
    version=version_ns['__version__'],
    description="""CAS authenticator for Jupyterhub""",
    long_description="",
    author="Carl (https://github.com/cwaldbieser)",
    author_email="",
    url="https://github.com/cwaldbieser/jhub_cas_authenticator",
    license="GPLv3",
    platforms="Linux, Mac OS X",
    keywords=['Interactive', 'Interpreter', 'Shell', 'Web'],
    classifiers=[
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    install_requires=[
        'alembic>=1.0.0',
        'async-generator>=1.8',
        'certifi>=2017.4.17',
        'chardet<3.1.0,>=3.0.2',
        'decorator>=4.3.0',
        'idna<2.8,>=2.5',
        'ipython-genutils>=0.2.0',
        'jinja2>=2.10',
        'jupyterhub>=0.9.2',
        'lxml>=lxml-4.3.0',
        'Mako>=1.0.7',
        'MarkupSafe>=0.23',
        'pamela>=0.3.0',
        'prometheus-client>=0.0.21',
        'python-dateutil>=2.7.3',
        'python-editor>=0.3',
        'python-oauth2>=1.0',
        'requests>=2.19.1',
        'six>=1.11.0',
        'SQLAlchemy>=1.1',
        'tornado>=5.1',
        'traitlets>=4.3.2',
        'urllib3<1.24,>=1.21.1',
    ]
)


def main():
    setup(**setup_args)


if __name__ == '__main__':
    main()
